import React, { Component } from 'react';
import Header from './components/Header.jsx';
import PlayerBoard from './components/PlayerBoard.jsx';
import BottomButtons from './components/BottomButtons.jsx';
import './App.css';


class App extends Component {

  state = {

    // Timer Values
    timerHours: 0,
    timerMinutes: 0,
    timerSeconds: 55,
    timerStarted: false,
    timerStopped: true,
    timerRef: null, // used to identify setInterval and used in clearInterval

    // Players array of objects
    // Eventually remove static objects to make players and empty array
    players: [{name: "Frank", score: 100},
              {name: "Bodine", score: 10},
              {name: "Cleophus", score: 30}
             ],
  }

  // Function that increments the timer
  incrementTimer = () => {

    // Declare timer variables
    let timer = this.state;
    let seconds = timer.timerSeconds;
    let minutes = timer.timerMinutes;

    if (!timer.timerStopped && timer.timerStarted) {
      this.setState({
        timerSeconds: timer.timerSeconds + 1,
      });

      if (seconds >= 59) {
        this.setState({
          timerMinutes: timer.timerMinutes + 1,
          timerSeconds: 0,
        });

        if (minutes >= 59) {
          this.setState({
            timerHours:  timer.timerHours + 1,
            timerMinutes: 0,
          });
        }
      }
    } else {

      // Exit out of the setInterval loop and the function if timerStopped is
      clearInterval(timer.timerRef);
      return;
    }
  }

  // Function to handle start button click event
  handleTimerStart = () => {
    this.setState({
      timerStopped: false,
      timerStarted: true,
      timerRef: setInterval(this.incrementTimer, 1000),
    });
  };

  // Function to handle stop button click event
  handleTimerStop = () => {
    if (!this.state.timerStopped) {
      this.setState({
        timerStopped: true,
        timerStarted: false,
      });
    } else {
      return;
    }
  };

  // Function to handle reset button click event
  handleTimerReset = () => {
    if (this.state.timerStarted) {
      // stop the timer by changing the state of timerStopped to true
      // set the seconds, minutes, and hours to zero
      this.setState({
        timerStarted: false,
        timerStopped: true,
        timerHours: 0,
        timerMinutes: 0,
        timerSeconds: 0,
      });
      // if timerStarted stopped already, reset timer values to zero
    } else if (!this.state.timerStarted && this.state.timerStopped) {
      this.setState({
        timerHours: 0,
        timerMinutes: 0,
        timerSeconds: 0,
      });
    }
  }

  // Add Player Click event handler
  // This button should open an input text field form
  // When the user begins typing, a save button should appear
  // The save button should disappear when the text field is empty
  // When the user clicks save button, the name should appear on the scoreboard and the save button and the input text field should disappear
  handleAddPlayer = (e) => {

    // remove "hide" class from input text field
    document.getElementById("add-player-field").classList.remove("hide");

    // Hide Add Players button
    document.getElementById("add-player-button").classList.add("hide");

    // Remove Hide class from Submit Name button
    document.getElementById("submit-player").classList.remove("hide");


    // if buttonClicked = true AND if user typed something into input field
      // change the name of button from "Add Player" to "Submit"
    /*
    // if user typed something into input field
    if (e.value) {
    // change the name of button from "Add Player" to "Submit"
  }

    */

  }

  handleSubmitName = () => {
    console.log("submit button was clicked");

    // Add user input name to Player Name array that is found in state; new players should have a default score of zero

    // playerName = "";
    // playerObject = {};

    // this button should "listen" for changes in input

    /* if (cursor is in input field AND playerName > 0) {
    store input into playerName
    let playerName = e.someShit;

  }*/
    // Store playerName into Object
    // let playerObject = {name: playerName, score: 0};
    /*
    STATE:
        players: [{name: "Frank", score: 0},
                  {name: "Bodine", score: 10}
                 ],

    this.setState({
    players: this.state.players.push(playerObject),
  });

    */

  }

  handleResetScore = () => {
    console.log("reset score button was clicked");
  }

  render() {
    return (
      <div className="container">
        <div className="App">
          <Header timer={this.state}
                  startButton={this.handleTimerStart}
                  stopButton={this.handleTimerStop}
                  resetButton={this.handleTimerReset}/>
          <PlayerBoard players={this.state.players}/>
          <BottomButtons addPlayer={this.handleAddPlayer}
                         resetScore={this.handleResetScore}
                         submitPlayer={this.handleSubmitName}/>
        </div>
      </div>
    );
  }
}

export default App;
