import React from 'react';
import './css/score.css';

const Score = (props) => {
  return (
    <div className="player-score">
      <button className="decrement-button">-</button>
      <h4 className="current-score">{props.playerScore}</h4>
      <button className="increment-button">+</button>
    </div>
  );
};

export default Score;

/*
when the score is 0, the decrement button should be disabled
*/
