import React from 'react';
import Player from './Player';
import './css/playerBoard.css';

function PlayerBoard(props) {
  return (
    <div className="player-board-container">
      <div className="playerboard-header">
        <h2 className="player-title">Player Name</h2>
        <h2 className="score-title">Score</h2>
      </div>
      <ul className="player-board">
        <Player players={props.players}/>
      </ul>
    </div>
  );
}

export default PlayerBoard;
