import React from 'react';
import './css/timer.css';

function Timer(props) {

  return (
    <div className="timer">
      <h4 className="timer-title">Timer</h4>
      <h4 className="clock">{
          props.timer.timerHours + " : " +
          props.timer.timerMinutes + " : " +
          props.timer.timerSeconds}
      </h4>
      <div className="timer-buttons">
        <button onClick={props.stopButton} className="stop">Stop</button>
        <button onClick={props.startButton} className="start">Start</button>
        <button onClick={props.resetButton} className="reset">Reset</button>
      </div>
    </div>
  );
}

export default Timer;
