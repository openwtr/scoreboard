import React from 'react';
import Timer from './Timer.jsx';
import './css/header.css';

function Header(props) {
  return (
    <div className="header-container">
      <h1>Scoreboard</h1>
      <Timer timer={props.timer}
             startButton={props.startButton}
             stopButton={props.stopButton}
             resetButton={props.resetButton}/>
    </div>
  );
}

export default Header;
