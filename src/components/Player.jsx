import React from 'react';
import Score from './Score.jsx';
import './css/player.css';

function Player(props) {
  console.log(props.players);

  // Loop through array and create a list item for iteration
  let playersListItem = props.players.map((player) => {
    const KEY = 666;

    return (
      <li key={KEY * Math.random()} className="player-list-item-container">
        <div className="player-name-container">
          {/*create click event handler*/}
          <button className="delete-player">X</button>
          <h3 className="player-name">{player.name}</h3>
        </div>
        <Score playerScore={player.score}/>
      </li>
    );
  });

  // map through an array of players and create a li for each player
  // the li of player should appear on player board
  return (
    playersListItem
  );

}

export default Player;
