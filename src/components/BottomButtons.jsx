import React from 'react';
import './css/bottomButtons.css';

function BottomButtons(props) {
  return (
    <div className="bottom-buttons-container">
      <form id="add-player-field" className="hide">
        <input type="text"></input>
        <button
          onSubmit={props.submitPlayer}
          id="submit-player">Submit Name
        </button>
      </form>
      <div className="add-playerbutton-container">
        <button
          className=""
          onClick={props.addPlayer}
          id="add-player-button">Add Player
        </button>
      </div>
      <button
        onClick={props.resetScore}
        className="reset-score">Reset Score
      </button>
    </div>
  );
}

export default BottomButtons;
