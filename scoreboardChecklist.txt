Component:  App
{Build scoreboard container}
	+ create a box with a fixed width
	+ box should have rounded corners

{Input static info into scoreboard container}
	+ Create header with scoreboard title and static timer
	+ Add margin around timer buttons
	+ Vertically center scoreboard title
	+ Create static player board with score plus de/increment buttons
	+ Vertically center score number
	+ Center score over column
	+ Create three static buttons:  addPlayer, removePlayer (goes in front of player's name), resetScore
	+ add margin around addPlayer and resetScore buttons and center them horizontally on scoreboard
============================================================================
Component:  Header
{Build scoreboard title}
	+ create Header component in new directory called components
	+ add static title called "Scoreboard" to Header component
	+ add static timer to Header component
	+ export Header component
	+ add Header component to App component
============================================================================
Component:  Timer
	// Completed tasks
	+ lord hammercy...how we gone do this shit bitch?
	+ create Timer branch
	+ create Timer component
	+ figure out how to display time
	+ add functionality to start Button (add click events)
	+ find out why after setState, timerSeconds are undefined
	+ get timer to update the seconds


	+ second should restart once it reaches 60
	+ after seconds reach 60, the minutes should increment by one
	+ after 60 minutes, the hours should increment by 1
	+ create a button that will stop the timer from increment
